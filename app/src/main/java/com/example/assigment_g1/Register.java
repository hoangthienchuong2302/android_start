package com.example.assigment_g1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.lab3.R;

public class Register extends AppCompatActivity {
     EditText edtUser,edtPass;
    Button btnRegister,btnLogin;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        edtUser=findViewById(R.id.edtUser);
        edtPass=findViewById(R.id.edtPass);
        btnLogin=findViewById((R.id.btnLogin));
        btnRegister=findViewById(R.id.btnRegister);


        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent r = new Intent(Register.this,login.class);
                String userName=edtUser.getText().toString();
                String passWord=edtPass.getText().toString();
                Bundle bundle=new Bundle();
                bundle.putString("user",userName);
                bundle.putString("pass",passWord);
                r.putExtra("data",bundle);
                startActivity(r);
            }
        });
    }
}
