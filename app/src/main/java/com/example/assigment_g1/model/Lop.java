package com.example.assigment_g1.model;

public class Lop {
    public String maLop;
    public String lopName;
    public Lop() {
    }

    public Lop(String maLop, String lopName) {
        this.maLop = maLop;
        this.lopName = lopName;
    }
}