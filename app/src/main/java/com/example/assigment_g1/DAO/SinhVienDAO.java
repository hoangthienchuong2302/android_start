package com.example.assigment_g1.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.assigment_g1.database.DbHelper;
import com.example.assigment_g1.model.SinhVien;

import java.util.ArrayList;
import java.util.List;

public class SinhVienDAO {
    private SQLiteDatabase db;

    public SinhVienDAO(Context context) {
        DbHelper dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    public long insert(SinhVien item) {

        ContentValues values = new ContentValues();
        values.put("maSV", item.maSV);
        values.put("maLop", item.maLop);
        values.put("svName", item.svName);


        return db.insert("sinhvien", null, values);
    }

    public int update(SinhVien item) {
        ContentValues values = new ContentValues();
        values.put("svName", item.svName);
        values.put("maLop", item.maLop);
        return db.update("sinhvien", values, "maSV=?", new String[]{item.maSV});
    }

    public int delete(String maSV) {
        return db.delete("sinhvien", "maSV=?", new String[]{maSV});
    }

    //get nhieu tham so
    public List<SinhVien> get(String sql, String...selectionArgs) {

        List<SinhVien> list = new ArrayList<SinhVien>();

        Cursor c = db.rawQuery(sql, selectionArgs);

        while (c.moveToNext()){
            SinhVien item = new SinhVien();
            item.maSV = c.getString(c.getColumnIndex("maSV"));
            item.svName = c.getString(c.getColumnIndex("svName"));
            item.maLop = c.getString(c.getColumnIndex("maLop"));
            list.add(item);
        }

        return list;
    }

//get tat ca lop

    public List<SinhVien> getAll() {
        String sql = "SELECT * FROM sinhvien";

        return get(sql);
    }


//get Lop theo lopId

    public SinhVien getId(String maSV) {

        String sql = "SELECT * FROM sinhvien WHERE maSV=?";

        List<SinhVien> list = get(sql, maSV);

        return list.get(0);
    }
    public  List<SinhVien> getLopId(String id){
        List<SinhVien> list= new ArrayList<SinhVien>();
        String sql = "SELECT * FROM sinhvien WHERE maLop=?";
        list=get(sql,id);
        return list;
    }
}
