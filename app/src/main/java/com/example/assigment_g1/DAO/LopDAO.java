package com.example.assigment_g1.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.assigment_g1.database.DbHelper;
import com.example.assigment_g1.model.Lop;

import java.util.ArrayList;
import java.util.List;

public class LopDAO {
    private SQLiteDatabase db;

    public LopDAO(Context context) {
        DbHelper dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    public long insert(Lop item) {

        ContentValues values = new ContentValues();
        values.put("maLop", item.maLop);
        values.put("lopName", item.lopName);

        return db.insert("lop", null, values);
    }

    public int update(Lop item) {
        ContentValues values = new ContentValues();

        values.put("lopName", item.lopName);

        return db.update("lop", values, "maLop=?", new String[]{item.maLop});
    }

    public int delete(String maLop) {
        return db.delete("lop", "maLop=?", new String[]{maLop});
    }

    //get nhieu tham so
    public List<Lop> get(String sql, String...selectionArgs) {

        List<Lop> list = new ArrayList<Lop>();

        Cursor c = db.rawQuery(sql, selectionArgs);

        while (c.moveToNext()){
            Lop item = new Lop();
            item.maLop = c.getString(c.getColumnIndex("maLop"));
            item.lopName = c.getString(c.getColumnIndex("lopName"));
            list.add(item);
        }

        return list;
    }

//get tat ca lop

    public List<Lop> getAll() {
        String sql = "SELECT * FROM lop";

        return get(sql);
    }


//get Lop theo lopId

    public Lop getId(String maLop) {

        String sql = "SELECT * FROM lop WHERE maLop=?";

        List<Lop> list = get(sql, maLop);

        return list.get(0);
    }
}
