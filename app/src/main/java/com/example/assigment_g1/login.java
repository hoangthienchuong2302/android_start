package com.example.assigment_g1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.lab3.R;

public class login extends AppCompatActivity {

    EditText edtUser,edtPass;
    Button btnLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        edtUser = findViewById(R.id.edtUser);
        edtPass = findViewById(R.id.edtPass);
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = edtUser.getText().toString();
                String pass = edtPass.getText().toString();
                Intent i= getIntent();
                Bundle bundle = i.getBundleExtra("data");
                String isUser =bundle.getString("user",null);
                String isPass = bundle.getString("pass",null);
                if(user.equalsIgnoreCase(isUser) && pass.equals(isPass))
                {
                    Intent r = new Intent(login.this,ChucNang.class);
                    r.putExtra("user",user);
                    startActivity(r);
                    Toast.makeText(login.this, "Đăng nhập thành công", Toast.LENGTH_SHORT).show();
                    Toast.makeText(login.this, "Bạn nhập sai User hoặc password", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
