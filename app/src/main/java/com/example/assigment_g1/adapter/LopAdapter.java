package com.example.assigment_g1.adapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import com.example.assigment_g1.model.Lop;
import com.example.assigment_g1.xemDS;
import com.example.lab3.R;

import java.util.ArrayList;
public class LopAdapter extends ArrayAdapter<Lop> {
    private Context context;
    private ArrayList<Lop> lists;
    TextView txtmaLop, txtName;
    ImageView imgDel;
    public LopAdapter(@NonNull Context context, ArrayList<Lop> lops) {
        super(context, 0,lops);
        this.context=context;
        this.lists=lops;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;

        if (v == null){
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.item_list_lop, null);
        }

        final Lop item = lists.get(position);
        if(item != null){

            txtmaLop = v.findViewById(R.id.txtMaLop);
            txtmaLop.setText(String.valueOf(item.maLop));
            txtName = v.findViewById(R.id.txtName);
            txtName.setText(item.lopName);
            imgDel=v.findViewById(R.id.imgDelete);
        }



        imgDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               ((xemDS)context).xoa(item.maLop);

            }
        });

        return v;
    }
}
