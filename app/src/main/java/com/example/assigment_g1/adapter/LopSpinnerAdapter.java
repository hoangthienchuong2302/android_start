package com.example.assigment_g1.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.assigment_g1.model.Lop;
import com.example.lab3.R;

import java.util.ArrayList;

public class LopSpinnerAdapter extends ArrayAdapter<Lop> {

    private Context context;
    private ArrayList<Lop> lists;
    TextView tvLopId, tvLopName;
    public LopSpinnerAdapter(@NonNull Context context, ArrayList<Lop> lops) {
        super(context, 0,lops);
        this.context=context;
        this.lists=lops;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View v = convertView;

        if (v == null){
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.item_spinner, null);
        }

        final Lop item = lists.get(position);
        if(item != null){

            tvLopId = v.findViewById(R.id.tvLopId);
            tvLopId.setText(item.maLop+". ");

            tvLopName = v.findViewById(R.id.tvLopTen);
            tvLopName.setText(item.lopName);


        }





        return v;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;

        if (v == null){
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.item_spinner, null);
        }

        final Lop item = lists.get(position);
        if(item != null){

            tvLopId = v.findViewById(R.id.tvLopId);
            tvLopId.setText(item.maLop+". ");

            tvLopName = v.findViewById(R.id.tvLopTen);
            tvLopName.setText(item.lopName);


        }

        return v;
    }
}
