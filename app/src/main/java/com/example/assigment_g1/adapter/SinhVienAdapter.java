package com.example.assigment_g1.adapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import com.example.assigment_g1.ChucNang;
import com.example.assigment_g1.QuanLy;
import com.example.assigment_g1.model.Lop;
import com.example.assigment_g1.model.SinhVien;
import com.example.assigment_g1.xemDS;
import com.example.lab3.R;

import java.util.ArrayList;
public class SinhVienAdapter extends ArrayAdapter<SinhVien> {
    private Context context;
    private ArrayList<SinhVien> lists;
    TextView  txtName,txtMaSV,txtMaLop;
    ImageView imgDel;
    public SinhVienAdapter(@NonNull Context context, ArrayList<SinhVien> lops) {
        super(context, 0,lops);
        this.context=context;
        this.lists=lops;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;

        if (v == null){
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.item_list_sinhvien, null);
        }

        final SinhVien item = lists.get(position);
        if(item != null){
            txtName = v.findViewById(R.id.txtName);
            txtName.setText(item.svName);
            txtMaSV = v.findViewById(R.id.txtMaSV);
            txtMaSV.setText(item.maSV);
            txtMaLop = v.findViewById(R.id.txtMaLop);
            txtMaLop.setText(item.maLop);
            imgDel=v.findViewById(R.id.imgDelete);
        }

        
        imgDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((QuanLy)context).xoa(item.maSV);

            }
        });

        return v;
    }
}
