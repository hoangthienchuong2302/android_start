package com.example.assigment_g1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.lab3.R;

public class ChucNang extends AppCompatActivity {
final Context context=this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Button btnAdd, btnView, btnQuanLy;

        setContentView(R.layout.activity_chuc_nang);
        EditText edtHello = findViewById(R.id.edtHello);
        btnAdd = findViewById(R.id.btnAdd);
        btnView = findViewById(R.id.btnView);
        btnQuanLy = findViewById(R.id.btnQuanLy);
        Intent i = getIntent();
        String User = i.getStringExtra("user");
        edtHello.setText("WELCOME " + User + "!!!");

        btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ChucNang.this,xemDS.class);
                startActivity(i);
            }
        });
        btnQuanLy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent j = new Intent(ChucNang.this,QuanLy.class);
                startActivity(j);
            }
        });
    }
}
