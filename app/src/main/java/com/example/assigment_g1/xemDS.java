package com.example.assigment_g1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.assigment_g1.DAO.LopDAO;
import com.example.assigment_g1.adapter.LopAdapter;
import com.example.assigment_g1.model.Lop;
import com.example.lab3.R;

import java.util.ArrayList;

public class xemDS extends AppCompatActivity {

    LopAdapter adapter;
    EditText edtMaLop,edtTenLop;
    ListView lv;
    Button btnAdd, btnCancel,btnSave;
    Dialog dialog;
    ArrayList<Lop> list;
    LopDAO dao;
    Lop item;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xem_d_s);
        lv = findViewById(R.id.lv1);
        btnAdd = findViewById(R.id.btnAdd);
        dao = new LopDAO(this);

        capNhatLv();

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog(xemDS.this,0);
            }
        });
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                item = list.get(position);
                openDialog(xemDS.this,1);

                return false;
            }
        });
    }

    protected void openDialog(final Context context, final int type){
        //custom dialog
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.customdialog_lop);
        edtMaLop = dialog.findViewById(R.id.edtMaLop);
        edtTenLop = dialog.findViewById(R.id.edtTenLop);
        btnCancel = dialog.findViewById(R.id.btnCacel);
        btnSave = dialog.findViewById(R.id.btnSave);
        //kiem tra type insert 0 hay Update 1
        if (type != 0){
            edtMaLop.setText(item.maLop);
            edtMaLop.setEnabled(false);
            edtTenLop.setText(item.lopName);
        }
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item = new Lop();
                item.maLop=edtMaLop.getText().toString();
                item.lopName=edtTenLop.getText().toString();

                if (type==0){
                    //type = 0 (insert)
                    dao.insert(item);
                }else {
                    //type =1 (update)
                    dao.update(item);
                }

                capNhatLv();
                Toast.makeText(context, "Save", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    public void xoa(final String lopId){
        //Su dung Alert
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete");
        builder.setMessage("Ban co muon xoa khong?");
        builder.setCancelable(true);

        builder.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Goi function Delete
                        dao.delete(lopId);
                        //cap nhat listview
                        capNhatLv();
                        dialog.cancel();
                    }
                });

        builder.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        builder.show();

    }

    void capNhatLv(){
        list=(ArrayList<Lop> )dao.getAll();
        adapter = new LopAdapter(this,list);
        lv.setAdapter(adapter);
    }
}
