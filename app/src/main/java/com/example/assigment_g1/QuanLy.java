package com.example.assigment_g1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;


import com.example.assigment_g1.DAO.LopDAO;
import com.example.assigment_g1.DAO.SinhVienDAO;
import com.example.assigment_g1.adapter.LopSpinnerAdapter;
import com.example.assigment_g1.adapter.SinhVienAdapter;
import com.example.assigment_g1.model.Lop;
import com.example.assigment_g1.model.SinhVien;
import com.example.lab3.R;

import java.util.ArrayList;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.V;

public class QuanLy extends AppCompatActivity {
    SinhVienAdapter adapter;
    EditText edtMaSV,edtTenSV;

    Spinner spinner;
    LopSpinnerAdapter LopSpinnerAdapter;
    ArrayList<Lop> listLop;
    LopDAO lopDAO;
    Lop Lop;
    String maLop;

    ListView lv;
    Button btnThem, btnCancel,btnSave;
    Dialog dialog;
    ArrayList<SinhVien> list;
    SinhVienDAO dao;
    SinhVien item;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quan_ly);

        lv = findViewById(R.id.lv1);
        btnThem = findViewById(R.id.btnThem);
        dao = new SinhVienDAO(this);
        capNhatLv();



        btnThem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog(QuanLy.this,0);
            }
        });
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                item = list.get(position);
                openDialog(QuanLy.this,1);

                return false;
            }
        });
    }

    protected void openDialog(final Context context, final int type){
        //custom dialog
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.customdialog_sinhvien);

        edtMaSV = dialog.findViewById(R.id.edtMaSv);
        edtTenSV = dialog.findViewById(R.id.edtTenSV);

        btnCancel = dialog.findViewById(R.id.btnCacel);
        btnSave = dialog.findViewById(R.id.btnSave);
        spinner=dialog.findViewById(R.id.spinner);

        listLop=new ArrayList<Lop>();
        lopDAO= new LopDAO(context);
        listLop=(ArrayList<Lop>)lopDAO.getAll();

        LopSpinnerAdapter = new LopSpinnerAdapter(context,listLop);

        spinner.setAdapter(LopSpinnerAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                maLop=listLop.get(position).maLop;
                Toast.makeText(context, "Chọn "+listLop.get(position).maLop, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        //kiem tra type insert 0 hay Update 1
        if (type != 0){
            edtMaSV.setText(item.maSV);
            edtMaSV.setEnabled(false);
            edtTenSV.setText(item.svName);
        }
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item = new SinhVien();
                item.maSV=edtMaSV.getText().toString();
                item.svName=edtTenSV.getText().toString();
                item.maLop=maLop;
                dao= new SinhVienDAO(QuanLy.this)  ;
                if (type==0){
                    //type = 0 (insert)
                    dao.insert(item);
                }else {
                    //type =1 (update)
                    dao.update(item);
                }

                capNhatLv();
                Toast.makeText(context, "Save", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    public void xoa(final String lopId){
        //Su dung Alert
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete");
        builder.setMessage("Ban co muon xoa khong?");
        builder.setCancelable(true);

        builder.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Goi function Delete
                        dao.delete(lopId);
                        //cap nhat listview
                        capNhatLv();
                        dialog.cancel();
                    }
                });

        builder.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        builder.show();

    }

    void capNhatLv(){
        list=(ArrayList<SinhVien> )dao.getAll();
        adapter = new SinhVienAdapter(this,list);
        lv.setAdapter(adapter);
    }
}
