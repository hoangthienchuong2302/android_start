package com.kietlpt.mmob1032_asm_demo.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.kietlpt.mmob1032_asm_demo.database.DbHelper;
import com.kietlpt.mmob1032_asm_demo.model.Lop;

import java.util.ArrayList;
import java.util.List;

public class LopDAO  {
    private SQLiteDatabase db;

    public LopDAO(Context context) {
        DbHelper dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    public long insert(Lop item) {

        ContentValues values = new ContentValues();
        values.put("lopId", item.lopId);
        values.put("lopName", item.lopName);
        values.put("lopInfo", item.lopInfo);

        return db.insert("lop", null, values);
    }

    public int update(Lop item) {
        ContentValues values = new ContentValues();

        values.put("lopName", item.lopName);
        values.put("lopInfo", item.lopInfo);

        return db.update("lop", values, "lopId=?", new String[]{item.lopId});
    }

    public int delete(String maLop) {
        return db.delete("lop", "lopId=?", new String[]{maLop});
    }

    //get nhieu tham so
    public List<Lop> get(String sql, String...selectionArgs) {

        List<Lop> list = new ArrayList<Lop>();

        Cursor c = db.rawQuery(sql, selectionArgs);

        while (c.moveToNext()){
            Lop item = new Lop();
            item.lopId = c.getString(c.getColumnIndex("lopId"));
            item.lopName = c.getString(c.getColumnIndex("lopName"));
            item.lopInfo = c.getString(c.getColumnIndex("lopInfo"));
            list.add(item);
        }

        return list;
    }

//get tat ca lop

    public List<Lop> getAll() {
        String sql = "SELECT * FROM lop";

        return get(sql);
    }


//get Lop theo lopId

    public Lop getId(String maLop) {

        String sql = "SELECT * FROM Loaisach WHERE maLoai=?";

        List<Lop> list = get(sql, maLop);

        return list.get(0);
    }
}
