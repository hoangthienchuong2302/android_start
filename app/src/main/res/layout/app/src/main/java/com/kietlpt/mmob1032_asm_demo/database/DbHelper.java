package com.kietlpt.mmob1032_asm_demo.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DbHelper extends SQLiteOpenHelper {
    static final String DbName = "Demo_ASM";
    static final int Version = 2;
    public DbHelper(@Nullable Context context) {
        super(context, DbName, null, Version);
    }
    // Tao bang
    @Override
    public void onCreate(SQLiteDatabase db) {
        // Tao chuoi lenh
        String sqlLop = "CREATE TABLE lop(" +
                "lopId TEXT PRIMARY KEY," +
                "lopName TEXT NOT NULL,"+
                "lopInfo TEXT);";

        // chay lenh sql
        db.execSQL(sqlLop);

        String sqlSinhVien ="CREATE TABLE sinhvien(" +
                "svId TEXT PRIMARY KEY," +
                "svName TEXT NOT NULL," +
                "svNS TEXT," +
                "lopId TEXT REFERENCES lop(lopId));";
        db.execSQL(sqlSinhVien);

    }
    // Khi tang version, se xoa bang va goi lai onCreate
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql1 = "DROP TABLE sinhvien ;";
        db.execSQL(sql1);
        String sql2 = "DROP TABLE lop ;";
        db.execSQL(sql2);

        onCreate(db);
    }
}
