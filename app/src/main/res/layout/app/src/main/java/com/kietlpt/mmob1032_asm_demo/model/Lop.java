package com.kietlpt.mmob1032_asm_demo.model;

public class Lop {
    public String lopId;
    public String lopName;
    public String lopInfo;

    public Lop() {
    }

    public Lop(String lopId, String lopName, String lopInfo) {
        this.lopId = lopId;
        this.lopName = lopName;
        this.lopInfo = lopInfo;
    }

}
