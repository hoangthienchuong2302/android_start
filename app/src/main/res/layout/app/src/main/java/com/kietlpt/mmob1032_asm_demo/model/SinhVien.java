package com.kietlpt.mmob1032_asm_demo.model;

public class SinhVien {
    public String svId;
    public String svName;
    public String svNS;
    int lopId;

    public SinhVien() {
    }

    public SinhVien(String svId, String svName, String svNS, int lopId) {
        this.svId = svId;
        this.svName = svName;
        this.svNS = svNS;
        this.lopId = lopId;
    }
}
