package com.kietlpt.mmob1032_asm_demo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.kietlpt.mmob1032_asm_demo.MainActivity;
import com.kietlpt.mmob1032_asm_demo.R;
import com.kietlpt.mmob1032_asm_demo.model.Lop;

import java.util.ArrayList;

public class LopAdapter extends ArrayAdapter<Lop> {

    private Context context;
    private ArrayList<Lop> lists;
    TextView tvLopId, tvLopName, tvLopInfo;
    ImageView imgDel;
    public LopAdapter(@NonNull Context context,ArrayList<Lop> lops) {
        super(context, 0,lops);
        this.context=context;
        this.lists=lops;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;

        if (v == null){
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.item_list, null);
        }

        final Lop item = lists.get(position);
        if(item != null){

            tvLopId = v.findViewById(R.id.txtLopId);
            tvLopId.setText(String.valueOf(item.lopId));
            tvLopName = v.findViewById(R.id.txtLopName);
            tvLopName.setText(item.lopName);
            tvLopInfo = v.findViewById(R.id.txtLopInfo);
            tvLopInfo.setText(item.lopInfo);
            imgDel=v.findViewById(R.id.imgDelete);
        }



        imgDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              ((MainActivity)context).xoa(item.lopId);

            }
        });

        return v;
    }
}
